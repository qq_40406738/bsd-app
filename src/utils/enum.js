export const list1 = Object.freeze([
   {
	      name: "接单",
	      val: "0",
	    },
	    {
	      name: "待预约",
	      val: "1",
		  children:[
			  {
					chilName:"全部",
					chilNal:0,
					content:"暂无待预约客户订单",
					explain:"",
			  },{
					chilName:"立即预约",
					chilNal:1,
					content:"暂无立即预约单",
					explain:"指的是您需要在2小时内预约的订单,还未超时",
			  },{
					chilName:"已超时",
					chilNal:2,
					content:"暂无超时单",
					explain:"指的是预约超市的订单,需要尽快完成预约",
			  },{
					chilName:"预约失败",
					chilNal:3,
					content:"暂无待确认上门单",
					explain:"指的是已联系过客户,未确定上门时间的单需尽快再次预约",
			  },{
					chilName:"未到货",
					chilNal:4,
					content:"暂无未到货单",
					explain:"指的是配送类订单,需要等到货后再和客户预约上门时间,非配送类,需要接单后立即预约",
			  },
		  ]
	    },
	    {
	      name: "服务中",
	      val: "2",
		  children:[
			  {
					chilName:"待提货",
					chilNal:0,
			  },{
					chilName:"待上门",
					chilNal:1,
			  },{
					chilName:"待完成",
					chilNal:2,
			  }
		  ]
	    },
	    {
	      name: " 完工/关闭",
	      val: "3",
		  children:[
			  {
					chilName:"待收款",
					chilNal:0,
			  },{
					chilName:"交易成功",
					chilNal:1,
			  },{
					chilName:"交易关闭",
					chilNal:2,
			  }
		  ]
	    },
	    {
	      name: " 意向单",
	      val: "4",
		  children:[
			  {
					chilName:"有意向",
					chilNal:0,
			  },{
					chilName:"已完成",
					chilNal:1,
			  },{
					chilName:"不感兴趣",
					chilNal:2,
			  },{
					chilName:"已关闭",
					chilNal:3,
			  }
		  ]
	    },
	    {
	      name: "挂起单",
	      val: "5",
		  children:[
			  {
					chilName:"待预约",
					chilNal:0,
			  },{
					chilName:"待提货",
					chilNal:1,
			  },{
					chilName:"待上门",
					chilNal:2,
			  },{
					chilName:"已关闭",
					chilNal:3,
			  }
		  ]
	    },
])

export const moreService = Object.freeze([
		{
	      name: "接单",
	      val: "0",
	    },
	    {
	      name: "待预约",
	      val: "1",
	    },
	    {
	      name: "待提货",
	      val: "2",
		  nal: "0",
	    },
	    {
	      name: "待上门",
	      val: "2",
		  nal: "1",
	    },
	    {
	      name: " 待完成",
	      val: "2",
		  nal: "2",
	    },
	    {
	      name: "待收款",
	      val: "3",
	      nal: "0",
	    },    {
	      name: "交易成功",
	      val: "3",
		  nal: "1",
	    },    {
	      name: "交易关闭",
	      val: "3",
	      nal: "2",
	    },  {
	      name: " 意向单",
	      val: "4",
	    },  {
	      name: "挂起单",
	      val: "5",
	    },
])