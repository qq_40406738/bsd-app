
//倒计时
function countDown(time) {
	var nowTime = +new Date(); // 返回的是当前时间总的毫秒数
	var inputTime = +new Date(time) * 1000; // 返回的是用户输入时间总的毫秒数

	var times = (inputTime - nowTime) / 1000; // times是剩余时间总的秒数
	var d = parseInt(times / 60 / 60 / 24); // 把秒数转换成天数  （parselnt把得到的数转换为整数）
	//d = d < 10 ? '0' + d : d;//这里为了让时间数好看一点，比如把4天改成04天，所以加了三元判定，下面也是如此；
	var h = parseInt(times / 60 / 60 % 24); //时
	// h = h < 10 ? '0' + h : h;
	var m = parseInt(times / 60 % 60); //分
	m = m < 10 ? '0' + m : m;
	var s = parseInt(times % 60); // 秒
	s = s < 10 ? '0' + s : s;
	let str = '';
	if (d == '00') {
		str = h + '小时'
	} else {
		d + '天' + h + '小时'
	}
	return d + '天' + h + '小时'; //返回函数计算出的值
}

//时间戳转化为时间
function timetrans(date, type) {
	var date = new Date(date); //如果date为13位不需要乘1000
	var Y = date.getFullYear() ;
	var M = (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1) ;
	var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate());
	var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
	var m = (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
	var s = (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());
	if (type == 1) {
		return Y + '-' + M + '-' + D + h + m + s;
	} else if (type == 2) {
		return M+'月'+D+'日' ;
	} else {
		return Y +'-' + M +'-' + D
	}

}
export default {
	countDown,
	timetrans
}