import Vue from 'vue'
import App from './App'
import './uni.promisify.adaptor'
import store from './store';
import uView from "uview-ui";
Vue.use(uView);

Vue.config.productionTip = false

//全局过滤器
import filter from '@/utils/filter.js'
Object.keys(filter).forEach(key=>{
	Vue.filter(key,filter[key])
})

App.mpType = 'app'

const app = new Vue({
  ...App,
  store
})
app.$mount()
