import QQMapWx from "../../utils/qqmap-wx-jssdk";

const city = {
  state: {
    address: uni.getStorageSync("city")
      ? JSON.parse(uni.getStorageSync("city"))
      : {},
  },
  actions: {
    // 获取当前定位
    getCurrentOrientation({ commit }) {
      return new Promise((resolve, reject) => {
        let location = {
          longitude: 0,
          latitude: 0,
          province: "",
          city: "",
          area: "",
          street: "",
          address: "",
        };
        // uni.getLocation({
        //   type: "gcj02",
        //   success(res) {
        //     location.longitude = res.longitude;
        //     location.latitude = res.latitude;
        //     // 腾讯地图API
        //     const qqmapsdk = new QQMapWx({
        //       key: "VIZBZ-4SQWU-RSMV4-GM7IH-XBBFE-T6BV4", //这里填写自己申请的key
        //     });
        //     qqmapsdk.reverseGeocoder({
        //       location,
        //       success(response) {
        //         let info = response.result;
        //         location.province = info.address_component.province;
        //         location.city = info.address_component.city;
        //         location.area = info.address_component.district;
        //         location.street = info.address_component.street;
        //         location.address = info.address;
        //         commit('SET_CITY', location);
        //         resolve()
        //       },
        //     });
        //   },
        //   fail(err) {
        //     console.log(err);
        //     reject(err);
        //   },
        // });
      });
    },
  },

  mutations: {
    SET_CITY: (state, city) => {
      console.log(city, 'city----');
      state.address = city;
      uni.setStorageSync('city', JSON.stringify(city));
    }
  }
};

export default city;