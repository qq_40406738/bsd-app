import Request from "./plugins/request";

const http = new Request({
  //接口请求地址
  baseUrl:
    process.env.NODE_ENV === "development"
      ? "http://132.232.72.201:48084"
      : "http://132.232.72.201:48084",

  //#ifdef H5
  baseUrl:
    process.env.NODE_ENV === "development"
      ? "http://132.232.72.201:48084"
      : location.origin,
  //#endif
  //设置请求头（如果使用报错跨域问题，可能是content-type请求类型和后台那边设置的不一致）
  header: {
    "Content-Type": "application/json;charset=UTF-8",
  },
  // 请求超时时间（默认6000）
  timeout: 30000,
  // 默认配置（可不写）
  config: {
    // 是否自动提示错误
    isPrompt: true,
    // 是否显示加载动画
    load: true,
    // 是否使用数据工厂
    isFactory: true,
    // ... 可写更多配置
  },
});

export const _getAppToken = () => {
  // #ifdef H5
  const token = ((window || global || {}))?.$android?.__APP_TOKEN__?.();
  return token;
  // #endif
  return undefined;
};

http.requestStart = function (options) {
  //请求前加入token todo
  const token = uni.getStorageSync("userdata").accessToken;
  options.header["Authorization"] = 'Bearer ' +  uni.getStorageSync("userdata").accessToken;
  options.header["tenant-id"] = "1";
  return options; // return false 表示请求拦截，不会继续请求
};
http.dataFactory = async function (res) {
  console.debug("接口响应数据", {
    url: res.url,
    resolve: res.response,
    header: res.header,
    data: res.data,
    method: res.method,
    res,
  });
  if (res.response.statusCode && res.response.statusCode === 200) {
    let httpData = res.response.data;
    if (typeof httpData === "string") {
      httpData = JSON.parse(httpData);
    }
    // 开始----------------------以下是示例-请认真阅读代码-----------------------开始
    //判断数据是否请求成功
    if (httpData.success || [200, "200", "0", 0].includes(httpData?.code)) {
      // 重点------判断接口请求是否成功，成功就返回成功的数据
      // ---重点---返回正确的结果(then接受数据)---重点---
      return Promise.resolve(httpData);
    } else if (typeof httpData.code === "undefined") {
      // console.log("此时，接口返回了一条垃圾数据");
      return Promise.resolve(httpData);
    } else if (httpData?.code == 401) {
      uni.redirectTo({
        url: "/pages/login/loginIndex",
      });
    }
    else {
      //其他错误提示
      if (res.isPrompt) {
        // 是否提示
        uni.showToast({
          title: `${
            httpData.info || httpData.msg || httpData.message || "未知错误"
          }`, // 重点------把接口返回的错误抛出显示
          icon: "none",
          duration: 3000,
        });
      }
      // ---重点---返回错误的结果(catch接受数据)----重点---
      return Promise.reject({
        statusCode: 0,
        errMsg:
          "【request】" +
          (httpData.data?.message ||
            httpData.data?.msg ||
            httpData.response?.data?.msg ||
            httpData.response?.data?.message ||
            httpData.info ||
            httpData.msg ||
            httpData.message),
      });
    }
    // 结束----------------------以上是示例-请认真阅读代码-----------------------结束
  } else {
    // 返回错误的结果(catch接受数据)
    console.debug("http error ", res);
    return Promise.reject({
      statusCode: res.response?.statusCode,
      errMsg:
        res.data?.message ||
        res.data?.msg ||
        res.response?.data?.msg ||
        res.response?.data?.message ||
        res.response?.errMsg ||
        "[FE-REQUEST] 未知异常",
    });
  }
};
// 错误回调（所有错误都在这里）
http.requestError = function (e) {
  if (e.statusCode === 0) {
    // throw e;
    uni.showToast({
      title: `API_404`,
      icon: "none",
    });
  } else if (e.statusCode === 401) {
    let routes = getCurrentPages(); // 获取当前打开过的页面路由数组
    let curRoute = routes?.[routes.length - 1]?.route; //获取当前页面路由
    console.log("curRoute", curRoute);
    if (curRoute === "pages/login/loginIndex" || curRoute === "pages/home/index") {
      console.log("已在当前页面，无需跳转");
      return;
    }
    uni.redirectTo({
      url: "/pages/login/loginIndex",
    });
  }
  if (e.statusCode === 404) {
    uni.showToast({
      title: `API_404`,
      icon: "none",
    });
  } else {
    // console.log('showToast', e);
    let message = e.errMsg || e.message || e.msg || "未知异常"
    if (message === 'request:ok') {
      message = '未知异常'
    }
    uni.showToast({
      title: `${message}`,
      duration: 3000,
      icon: "none",
    });
  }
};
export default http;
