import http from '../common/request';

// 获取用户基本信息
export async function getUserInfo() {
  return http.get('/app-api/bsd/app/getCurrentMasterInfo');
}