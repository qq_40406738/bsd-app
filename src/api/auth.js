import http from '../common/request';

/**
 * 登录
 */
export async function loginAuth(data) {
  return http.post(`/app-api/bsd/app/login?username=${data.username}&password=${data.password}`)
}

/**
 * 注册 填写个人信息
 */
export async function signUserInfo(data) {
  return http.post('/app-api/bsd/app/register', data);
}

/**
 * 获取钱包
 */
export async function wallet() {
  return http.get('/app-api/bsd/app/getMasterWallet');
}