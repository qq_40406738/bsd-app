import http from '../common/request';

/**
 * 获取服务分类
 */
export async function serviceSort(data) {
  return http.get('/app-api/bsd/app/getServiceClassificationList', data)
}

/**
 * 获取技能库
 */
export async function skillLibrary(params) {
  return http.get('/app-api/bsd/app/getSkillBankList', params)
}

/**
 * 添加师傅技能
 */
export async function addSkill(data) {
  return http.post('/app-api/bsd/app/addMasterSkill', data)
}

/**
 * 移除师傅技能
 */
export async function delSkill(data) {
  return http.post('/app-api/bsd/app/removeMasterSkill', data)
}

/**
 * 获取师傅技能
 */
export async function workSkill(data) {
  return http.get('/app-api/bsd/app/getMasterSkillList', data);
}