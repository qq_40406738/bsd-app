import http from '../common/request';
/**
 * 上传图片
 */
export async function uploadImageFile(filePath, file) {
  const baseUrl =
    process.env.NODE_ENV === "production"
      ? "http://132.232.72.201:48084"
      : "http://132.232.72.201:48084";
  return new Promise((resolve, reject) => {
    uni.uploadFile({
      url: baseUrl + "/app-api/bsd/app/upload",
      filePath,
      name: "file",
      header: {
        "tenant-id": 1,
        "Content-Type": "multipart/form-data",
        "Authorization": 'Bearer ' + uni.getStorageSync("userdata").accessToken
      },
      formData: {
        file
      },
      success: (res) => {
        const resData = JSON.parse(res.data);
        if (resData.code == 0) {
          resolve(resData);
        } else {
          reject(resData.message);
        }
      },
      fail: (err) => {
        reject(err);
      },
    });
  })
}

// 获取地址
export async function getRegionalTreeData() {
  return http.get('/app-api/bsd/wechat/getRegionalTreeData')
}