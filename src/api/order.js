import http from '../common/request';

// 获取订单列表
export async function getPushOrderPage(data) {
  return http.get('/app-api/bsd/app/getPushOrderPage',data);
}
// 获取订单详情
export async function getOrderDetailById(data) {
  return http.get('/app-api/bsd/app/getOrderDetailById',data);
}
// 获取订单剩余可报价数量
export async function getOrderQuoteRemainingNumber(data) {
  return http.get('/app-api/bsd/app/getOrderQuoteRemainingNumber',data);
}
// 师傅报价（订单）
export async function masterQuotes(data) {
  return http.post('/app-api/bsd/app/masterQuotes',data);
}
// 获取师傅今日剩余报价次数（订单）
export async function getMasterQuoteRemainingNumber(data) {
  return http.get('/app-api/bsd/app/getMasterQuoteRemainingNumber',data);
}
// 获取师傅报价服务承诺（订单中）
export async function getServiceCommitment(data) {
  return http.get('/app-api/bsd/app/getServiceCommitment',data);
}
// 获取担保协议（订单中）
export async function getSecurityAgreement(data) {
  return http.get('/app-api/bsd/app/getSecurityAgreement',data);
}